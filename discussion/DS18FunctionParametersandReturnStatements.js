console.log ("Hello, World!")


// Function Parameters and Arguments 

// "name" is called a parameter that acts as a named variable/container that exists only inside of the function. 
// "juana" - argument - it is the information/data provided directly into the function. 



function printInput (name){
	console.log ("My name is " + name);
};
printInput("Gemwiel")
printInput("Nicolas")

let sampleVariable = "Yui"; 
 
printInput(sampleVariable)

//==========

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments 

// Function parameters can also accept other functions as arguments.


function argumentFunction(){
	console.log ("This function was passed as an argument before the message was printed.");
}

function invokeFunction (argumentFunction){
	argumentFunction()
	console.log ("this code comes from invokeFunction")
}

invokeFunction(argumentFunction);

// Multiple Arguments - will correspond to the number of "Parameters" declared in a function in succeeding order. 

function createFullName (firstName, middleName, lastName){
	console.log (firstName + ' ' + middleName + ' ' + lastName);
}
createFullName ("Juan", "Dela");
createFullName ("Juan", "Dela", "Cruz");
createFullName ("Jane", "Dela", "Cruz", "Hello");

// The Return Statements - allows us to output a value from a function to be passed to the line/block of code that invoked/ called the function.


// The 'return' statement also stops the execution of the function and any code after the return statement will not be executed.

function returnFul1Name (firstNameOne, middleNameTwo, lastNameThree) 

	{ 
		return firstNameOne + " " + middleNameTwo + " " + lastNameThree

		console. Log ("This message will not be printed");
}

let completeName = returnFul1Name ('Joe ', 'Jayson ', 'Helberg');
console.log (completeName)

//====

function returnAddress (city, Province){
	let fulladdress = (city + ", " + Province)
	return fulladdress
}

let myAddress = returnAddress ("Bacoor", "Cavite")

console.log (myAddress)





